module load craype-accel-amd-gfx90a
module load amd/5.3.0
module load cce/15.0.1
export ROCM_PATH=/opt/rocm-5.3.0/

export MPICH_GPU_SUPPORT_ENABLED=1
export OMP_NUM_THREADS=1

if [[ -z $QEROOT ]]; then
export QEROOT=`pwd`
fi

export ESPRESSO_PSEUDO=$QEROOT/pseudo/

# Workaround needed for compiling QE with CCE 15.0
export CRAY_CCE_OPT_ARGS="--passes=default<O1>"

