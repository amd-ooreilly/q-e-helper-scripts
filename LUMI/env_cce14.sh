module --force purge
module load LUMI/22.08 partition/G
module load craype-x86-rome
module load libfabric
module load craype-network-ofi
module load perftools-base
module load cce/14.0.2
module load craype
module load cray-fftw
module load craype-accel-amd-gfx90a
module load PrgEnv-cray/8.3.3
module load rocm/5.3.3
export PE_MPICH_GTL_DIR_amd_gfx90a=${PE_MPICH_GTL_DIR_amd_gfx90a}
export PE_MPICH_GTL_LIBS_amd_gfx90a=${PE_MPICH_GTL_LIBS_amd_gfx90a}
export gcc_already_loaded=1
export CRAY_PRGENVGNU=loaded
export MPICH_GPU_SUPPORT_ENABLED=1
export MPI_INC=$CRAY_MPICH_BASEDIR/cray/10.0/include
export MPI_DIR=$CRAY_MPICH_BASEDIR/cray/10.0/lib
export PATH=$PATH:$CRAY_MPICH_BASEDIR/cray/10.0/bin
export ROCM_PATH=/opt/rocm/
export ESPRESSO_PSEUDO=../../pseudo/
export QEROOT=`pwd`
