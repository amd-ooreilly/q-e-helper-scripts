##!/bin/bash

# Helper functions used by job submission scripts

function set_para_prefix() {
# Patch run_pw.sh such that it uses srun and correctly binds ranks to GPUs
sed -i 's|export PARA_PREFIX=.*|export PARA_PREFIX="srun --cpu-bind map_cpu:49,57,17,25,1,9,33,41 --gpus $QE_USE_MPI -n $QE_USE_MPI ../select_gpu "|' run-pw.sh

}

function reset_para_prefix() {
    git checkout run-pw.sh
}

function create_select_gpu() {
# Helper program for mapping MPI ranks to GPUs and setting CPU thread affinities
cat << EOF > select_gpu
#!/bin/bash

export ROCR_VISIBLE_DEVICES=\$SLURM_LOCALID
exec \$*
EOF

chmod +x ./select_gpu
}

function rm_select_gpu() {
    rm -f ./select_gpu
}

