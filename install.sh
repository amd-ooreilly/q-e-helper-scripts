#!/usr/bin/bash
# Installation script for modifying and putting QE helper scripts in their rightful places

self=$PWD
system=$self/$1

if [[ -z $MAKEINC ]]; then
    MAKEINC=make.inc
fi

if [[ -z $ENVFILE ]]; then
    ENVFILE=env.sh
fi

function set_project() {
    sed -i "s/--account=.*/--account=$ACCOUNT/" $1
}

function install_qe() {
    echo "Installing QE scripts..."

    echo "Backing up current configuration..."
    pushd $QEROOT
        git=`git rev-parse --short HEAD`
        zip qe_config_$git.zip make.inc env.sh *.lsf
    popd

    echo "Copying files..."
    cp $system/$MAKEINC $QEROOT/make.inc
    cp $system/*.lsf $QEROOT/.
    cp $system/$ENVFILE $QEROOT/env.sh
    cp $system/qe.sh $QEROOT/.

    echo "Modifying job scripts..."
    for job in $QEROOT/*.lsf; do
        set_project $job
    done

    echo "Done."

}

function install_ausurf() {
    echo "Installing AUSURF scripts..."
    
    echo "Copying files..."
    cp $system/AUSURF/*.lsf $AUSURF/

    echo "Modifying job scripts..."
    for job in $AUSURF/*.lsf; do
        set_project $job
    done

    echo "Done."
}


if [[ -z $ACCOUNT ]]; then
    echo ACCOUNT not set. Existing...
    exit
fi


if [[ -z $1 ]]; then
    echo "System not set. Existing..."
    exit
fi


if [[ -z $QEROOT ]]; then
    exit
else
    install_qe
fi


if [[ -z $AUSURF ]]; then
    exit
else
    install_ausurf
fi
